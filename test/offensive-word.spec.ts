import supertest from 'supertest';
import app from '../src/app';
import sequelize from '../src/infrastructure/config/postgresql';
import { connectToDB, disconnectDB } from './../src/infrastructure/config/mongo';
import Container from 'typedi';
import { UserService } from '../src/domain/services/user.service';
import { User, UserType } from '../src/domain/entities/user.entity';
import { IdVO } from '../src/domain/vos/id.vo';
import { EmailVO } from '../src/domain/vos/user/email.vo';
import { PasswordVO } from '../src/domain/vos/user/password.vo';
import { Role, RoleVO } from '../src/domain/vos/user/role.vo';
import { OffensiveWordRepository } from '../src/domain/repositories/offensive-word.repository';
import { UserRepository } from '../src/domain/repositories/user.repository';
describe('offensive-word', () => {

	const server = supertest(app);
	let adminToken = '';

	beforeAll(async () => {

		await connectToDB();
		await sequelize.authenticate();

		const emailAdmin = 'admin@example.org';
		const passwordAdmin = 'password';
        
		const userService = Container.get(UserService);
		const userData: UserType = {
			id: IdVO.create(),
			email: EmailVO.create(emailAdmin),
			password: PasswordVO.create(passwordAdmin),
			role: RoleVO.create(Role.ADMIN)
		};

		await userService.persist(new User(userData));

		const responseLogin = await server.post('/api/login').type('application/json')
			.send({email: emailAdmin, password: passwordAdmin});

		adminToken = responseLogin.body.token;
		console.log(responseLogin.body);
	});

	afterAll(async () => {
		disconnectDB();
		const repoUser: UserRepository = Container.get('UserRepository');
		await repoUser.deleteAll();
		await sequelize.close();
	});

	afterEach(async () => {
		const repoOffensiveWord: OffensiveWordRepository = Container.get('OffensiveWordRepository');
		await repoOffensiveWord.deleteAll();
	});
    
	
	it('should create', async () => {
		const newoffensive = {
			word: 'caca',
			level: 2
		};
        
        
		await server.post('/api/offensive-word').type('application/json')
			.set('Authorization', `Bearer ${adminToken}`)
			.send(newoffensive).expect(201);
	});


});