import { ExtractJwt, StrategyOptions, Strategy } from 'passport-jwt';
import { UserService } from '../../domain/services/user.service';
import Container from 'typedi';
import { EmailVO } from '../../domain/vos/user/email.vo';
	
const secret = process.env.CRPYT_SECRET;

const opts: StrategyOptions = {
	
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: `${secret}` //ha de coincidir con la firma del token
};

export default new Strategy(opts, async(payload, done) => {
	try {
        
		const {email} = payload;
		const userService = Container.get(UserService);
		const user = await userService.getByEmail(EmailVO.create(email));
		if(user) {
			return done(null, user);
		}
		return done(null, false, {message: 'User not found'});
	} catch (err) {
		console.log(err);
	}
});