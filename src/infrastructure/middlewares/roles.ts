import { Role } from '../../domain/vos/user/role.vo';
import express from 'express';
import { User } from '../../domain/entities/user.entity';


export const hasRole = (roles: Role[]) => {

	return async (req: express.Request, 
		res: express.Response, 
		next: express.NextFunction): Promise<express.Response | void> => {

		const user = req.user as User;
		const roleUser = user.role.value;

		const allow = roles.some(role => roleUser === role);
		if(allow) {
			next();
			return;
		}
		return res.status(403).json({status: 'Not allowed'});
	};
};