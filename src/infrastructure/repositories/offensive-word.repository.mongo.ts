import { OffensiveWord, OffensiveWordType } from '../../domain/entities/offensive-word.entity';
import { OffensiveWordRepository } from '../../domain/repositories/offensive-word.repository';
import { OffensiveWordModel } from './offensive-word.schema';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { AnyObject } from 'mongoose';
import { LevelVO } from '../../domain/vos/offensive-word/level.vo';
import { WordVO } from '../../domain/vos/offensive-word/word.vo';


export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {
	async save(offensiveWord: OffensiveWord): Promise<void> {
		const newOffensiveWord = {
			id: offensiveWord.id.value,
			word: offensiveWord.word.value,
			level: offensiveWord.level.value
		};
		const offensiveWordModel = new OffensiveWordModel(newOffensiveWord);
		return offensiveWordModel.save();
	}

	async delete(id: IdVO): Promise<void> {
		await OffensiveWordModel.findOneAndRemove({id: id.value});
	}

	async update(offensiveWord: OffensiveWord): Promise<OffensiveWord> {
		const response = await OffensiveWordModel.findOneAndUpdate( {id: offensiveWord.id.value}, {word: offensiveWord.word.value, level: offensiveWord.level.value});
		const offensiveWordData: OffensiveWordType = {
			id: IdVO.createWithUUID(response.id),
			level: LevelVO.create(response.level),
			word: WordVO.create(response.word)
		};
		return new OffensiveWord(offensiveWordData);
	}

	async getAll(): Promise<OffensiveWord[]> {
		const allOffensiveWords = await OffensiveWordModel.find({});
		const allOffensiveModel = allOffensiveWords.map((ofModel: AnyObject) => {
			const offensiveWordData: OffensiveWordType = {
				id: IdVO.createWithUUID(ofModel.id),
				level: LevelVO.create(ofModel.level),
				word: WordVO.create(ofModel.word)
			};
			return new OffensiveWord(offensiveWordData);
		});
		return allOffensiveModel;
	}

	async get(id: IdVO): Promise<OffensiveWord | null> {
		const offensiveWord = await OffensiveWordModel.findOne({id: id.value});
		if(!offensiveWord){ return offensiveWord;}
		
		const offensiveWordData: OffensiveWordType = {
			id: IdVO.createWithUUID(offensiveWord.id),
			level: LevelVO.create(offensiveWord.level),
			word: WordVO.create(offensiveWord.word)
		};
		return new OffensiveWord(offensiveWordData);
	}

	async deleteAll(): Promise<void> {
		await OffensiveWordModel.deleteMany({});
	}
}

