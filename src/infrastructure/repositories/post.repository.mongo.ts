import { PostRepository } from '../../domain/repositories/post.repository';
import { Post, PostType } from '../../domain/entities/post.entity';
import { PostModel } from './post.schema';
import { AnyObject } from 'mongoose';
import { IdVO } from '../../domain/vos/id.vo';
import { TextVO } from '../../domain/vos/post/text.vo';
import { TitleVO } from '../../domain/vos/post/title.vo';

export class PostRepositoryMongo implements PostRepository {

	async save(post: Post): Promise<void> {
		const newPost = {
			id: post.id.value,
			author: post.author,
			title: post.title.value,
			text: post.text.value,
			comments: post.comments
		};

		const postModel = new PostModel(newPost);
		return postModel.save();
	}

	async getAll():Promise<Post[]> {
		const allPost = await PostModel.find({});
		const allPostModel = allPost.map((postModel: AnyObject) => {
			const postData: PostType = {
				id: IdVO.createWithUUID(postModel.id),
				author: postModel.author,
				text: TextVO.create(postModel.text),
				title: TitleVO.create(postModel.title),
				coments: postModel.Comment
			};
			return new Post(postData);
		});
		return allPostModel;
	}
}