import mongoose from 'mongoose';

const PostSchema = new mongoose.Schema({

	id: {
		type: String,
		required: true
	},
	author: {
		type: Object, 
		required: true
	},
	title: {
		type: String,
		required: true
	},
	text: {
		type: String,
		rquired: true
	},
	comments : {
		type: Array 
	}
});

const PostModel = mongoose.model('Posts', PostSchema);

export { PostModel };