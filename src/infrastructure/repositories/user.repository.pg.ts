import { User, UserType } from '../../domain/entities/user.entity';
import { UserRepository } from '../../domain/repositories/user.repository';
import { RoleVO } from '../../domain/vos/user/role.vo';
import { EmailVO } from '../../domain/vos/user/email.vo';
import { IdVO } from '../../domain/vos/id.vo';
import { PasswordVO } from '../../domain/vos/user/password.vo';
import { UserModel } from '../config/user.schema';


export class UserRepositoryPG implements UserRepository {
	async save(user: User): Promise<void> {
		console.log('repo');
		const id = user.id.value;
		const email = user.email.value;
		const password = user.password.value;
		const role = user.role.value;

		const userModel = UserModel.build({id, email, password, role});
		await userModel.save();
	}
	
	async getByEmail(email: EmailVO): Promise<User | null> {
		const user: any = await UserModel.findOne({where: {email: email.value}});
		if(!user) {
			return null;
		}

		const userData: UserType = {
			id: IdVO.createWithUUID(user.id),
			email: EmailVO.create(user.email),
			password: PasswordVO.create(user.password),
			role: RoleVO.create(user.role)
		};

		return new User(userData);
	}

	async deleteAll(): Promise<void> {
		await UserModel.destroy({
			where: {},
			truncate: true
		});
	}

}