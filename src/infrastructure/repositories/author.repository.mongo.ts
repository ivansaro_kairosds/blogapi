import { Author, AuthorType } from '../../domain/entities/author.entity';
import { AuthorRepository } from '../../domain/repositories/author.repository';
import { AuthorNameVO } from '../../domain/vos/author/authorName.vo';
import { AuthorNicknameVO } from '../../domain/vos/author/authorNickname.vo';
import { IdVO } from '../../domain/vos/id.vo';
import { AuthorModel } from './author.schema';


export class AuthorRepositoryMongo implements AuthorRepository {
	async save(author: Author): Promise<void> {
		try {
			const newAuthor = {
				id: author.id.value,
				name: author.name.value,
				nickname: author.nickname.value
			};
			const authorModel = new AuthorModel(newAuthor);
			return authorModel.save();
		} catch (error) {
			console.log(error);
		}
	}

	async get(id: IdVO): Promise<Author | null> {
		const author = await AuthorModel.findOne({id: id.value});
		if(!author){return author;}

		const authorData: AuthorType = {
			id: IdVO.createWithUUID(author.id),
			name: AuthorNameVO.create(author.name),
			nickname: AuthorNicknameVO.create(author.nickname)
		};
		return new Author(authorData);
	}
} 