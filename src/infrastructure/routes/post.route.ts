import express from 'express';
import Container from 'typedi';
import passport from 'passport';
import { User } from '../../domain/entities/user.entity';
import { PostRequest } from '../../application/usecases/post.request';
import { UserIdRequest } from '../../application/usecases/user.idrequest';
import { CreatePostUseCase } from '../../application/usecases/create-post.usecase';
import { GetAllPostUseCase } from '../../application/usecases/getAll-post.usecase';



const router = express.Router();



router.post('/api/post', passport.authenticate('jwt', {session: false}),
	async (req: express.Request, res: express.Response) => {
		try {
			const {title, text} = req.body;
			const user = req.user as User;
			
			const id: UserIdRequest = user.id.value;
			const postRequest: PostRequest = {
				title, text
			};

			const useCase = Container.get(CreatePostUseCase);
			const postResponse = await useCase.execute(id, postRequest);
			return res.status(201).send(postResponse);

		} catch (error) {
			console.log(error);
		}
	});


router.get('/api/post/',
	async (req: express.Request, res: express.Response) => {
		try {
			const usecase = Container.get(GetAllPostUseCase);
			const items = await usecase.execute();
			res.json(items);
		} catch (error) {
			console.log(error);
		}
	});

export {router as PostRouter};