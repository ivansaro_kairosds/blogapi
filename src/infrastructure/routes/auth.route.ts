import express from 'express';
import { body, validationResult } from 'express-validator';
import Container from 'typedi';
import { SignInUseCase } from '../../application/usecases/sign-in.usecase';
import { SignUpAuthorCase } from '../../application/usecases/sign-up-author.usecase';
import { SignUpUserUseCase } from '../../application/usecases/sign-up-user.usecase';
import { IdVO } from '../../domain/vos/id.vo';
import { logger } from '../config/logger';

const router = express.Router();

router.post('/api/login', 
	body('email').notEmpty(), 
	body('password').notEmpty(), 
	async (req: express.Request, res: express.Response) => {
		try {
			logger.debug('se ve?');
			
			const errors = validationResult(req);
			if(!errors.isEmpty()) {
				return res.status(400).json({errors: errors.array()});
			}

			const useCase = Container.get(SignInUseCase);
			const { email, password } = req.body;
			const token = await useCase.execute({email,password});
			if(token) {
				res.status(200).json({token});
			}else {
				res.status(401).json({error: 'Not permitted'});
			}
		} catch (err) {
			return res.status(err.code).json({error: err.message});
		}
	});

router.post('/api/sign-up',
	body('email').notEmpty(), 
	body('password').notEmpty(),
	body('name').notEmpty(),
	body('nickname').notEmpty(),   
	async (req: express.Request, res: express.Response) => {
		try {
			
			const errors = validationResult(req);
			if(!errors.isEmpty()) {
				return res.status(400).json({errors: errors.array()});
			}  
			const id = IdVO.create();
            
			const useCaseUser = Container.get(SignUpUserUseCase);
			const useCaseAuthor = Container.get(SignUpAuthorCase);
			const {email,password, name, nickname} = req.body;

			await useCaseUser.execute({id, email,password});
			await useCaseAuthor.execute({id, name, nickname});

			res.status(201).json({status: 'Created'});
		} catch (err) {
			return res.status(err.code).json({error: err.message});
		}
	}
);



export { router as authRouter };