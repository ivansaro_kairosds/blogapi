
import express from 'express';
import { CreateOffensiveWordUseCase } from '../../application/usecases/create-offensive-word.usecase';
import { DeleteOffensiveWordUseCase } from '../../application/usecases/delete-offensive-word.usecase';
import { UpdateOffensiveWordUseCase } from '../../application/usecases/update-offensive-word.usecase';
import { OffensiveWordRequest } from '../../application/usecases/offensive-word.request';
import { GetOneOffensiveWordUseCase } from '../../application/usecases/getOne-offensive-word.usecase';
import { GetAllOffensiveWordUseCase } from '../../application/usecases/getAll-offensive-word.usecase';

import Container from 'typedi';
import { body, validationResult } from 'express-validator';
import { IdRequest } from '../../application/usecases/offensive-word.idrequest';
import passport from 'passport';
import { hasRole } from '../middlewares/roles';
import { Role } from '../../domain/vos/user/role.vo';


const router = express.Router();


/**
 * @openapi
 * /offensive-word:
 *   get:
 *     description: Get all offensive words
 *     responses:
 *       200:
 *         description: Returns all offensive words.
 *       401:
 *         description: Unauthorized.
 */

router.get('/api/offensive-word/:id', passport.authenticate('jwt', {session: false}), hasRole([Role.ADMIN]),
	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if(!errors.isEmpty()) {
				console.log('error');
				return res.status(400).json({errors: errors.array()});
			}
			const id = req.params.id;
			const usecase = Container.get(GetOneOffensiveWordUseCase);
			const item = await usecase.execute(id);
			res.json(item); 
		} catch (error) {
			console.log(error);
		}
	});

router.get('/api/offensive-word/', passport.authenticate('jwt', {session: false}), hasRole([Role.ADMIN]),
	async (req: express.Request, res: express.Response) => {
		try {
			const usecase = Container.get(GetAllOffensiveWordUseCase);
			const items = await usecase.execute();
			res.json(items);
		} catch (error) {
			console.log(error);
		}
	});

router.delete('/api/offensive-word/:id', passport.authenticate('jwt', {session: false}),hasRole([Role.ADMIN]),
	async (req: express.Request, res: express.Response) => {
		try {
			const useCase = Container.get(DeleteOffensiveWordUseCase);
			const id: IdRequest = req.params.id;
			await useCase.execute(id);
			return res.send('Deleted!');

		} catch (err) {
			return res.status(err.code).json({error: err.message});
		}
	});


router.post('/api/offensive-word/', 
	passport.authenticate('jwt', {session: false}), hasRole([Role.ADMIN]),
	body('word').notEmpty().trim(),
	body('level').notEmpty().isNumeric(),
	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if(!errors.isEmpty()) {
				console.log('error');
				return res.status(400).json({errors: errors.array()});
			} 
			
			const { word, level} = req.body;
			const offensiveWordRequest: OffensiveWordRequest = {
				word, level
			};
			const useCase = Container.get(CreateOffensiveWordUseCase);
			const offensiveWordResponse = await useCase.execute(offensiveWordRequest);
			return res.status(201).send(offensiveWordResponse);
		} catch (error) {
			console.log(error);
		}
	});


router.put('/api/offensive-word/:id', 
	passport.authenticate('jwt', {session: false}), hasRole([Role.ADMIN]),
	async (req: express.Request, res: express.Response) => {
		
		try {
			const errors = validationResult(req);
			if(!errors.isEmpty()) {
				console.log('error');
				return res.status(400).json({errors: errors.array()});
			}  
			const { word, level} = req.body;
			const id: IdRequest = req.params.id;
			const offensiveWordRequest: OffensiveWordRequest = { word, level };
			const useCase = Container.get(UpdateOffensiveWordUseCase);
			const offensiveWordResponse = await useCase.execute(id, offensiveWordRequest);
			return res.status(201).send(offensiveWordResponse);
		} catch (err) {
			return res.status(err.code).json({error: err.message});

		}
	});


export {router as offensiveWordRouter};