import mongoose, { disconnect } from 'mongoose';
import { logger } from './logger';

const connectToDB = async () => {
	try{
		const host = process.env.MONGO_HOST ?? 'localhost';
		const port = process.env.MONGO_PORT;
		const user = process.env.MONGO_USER;
		const password = process.env.MONGO_PASSWORD;
		logger.error(`este es el host ${host}`);
		
		
		
		await mongoose.connect(`mongodb://${host}:${port}/blog`, {
			authSource:'admin',
			auth: {
				user: `${user}`,
				password: `${password}`,
			},
			useUnifiedTopology: true,
			useNewUrlParser: true,
			useFindAndModify: false
		});
	}catch(err) {
		console.log(err);
	}
};

const disconnectDB = () => {
	mongoose.disconnect();
};

export { connectToDB, disconnectDB };