import { UserService } from '../../domain/services/user.service';
import { User, UserType } from '../../domain/entities/user.entity';
import { IdVO } from '../../domain/vos/id.vo';
import { EmailVO } from '../../domain/vos/user/email.vo';
import { PasswordVO } from '../../domain/vos/user/password.vo';
import { Role, RoleVO } from '../../domain/vos/user/role.vo';
import Container from 'typedi';

const populate = async(): Promise<void> => {

	const userService = Container.get(UserService); 
	const userData: UserType = {
		id: IdVO.create(),
		email: EmailVO.create('admin@xample.org'),
		password: PasswordVO.create('password'),
		role: RoleVO.create(Role.ADMIN)
	};
	//UserModel.sync();
	await userService.persist(new User(userData));
};

export {populate as populateDatabases};