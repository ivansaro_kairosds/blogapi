import { Sequelize } from 'sequelize';

const port = process.env.PG_PORT;
const user = process.env.PG_USER;
const password = process.env.PG_PASSWORD;

const sequelize = new Sequelize(`postgres://${user}:${password}@localhost:${port}/pgdb`);

const connectToPostGres = async() => {
	try {
		await sequelize.authenticate()
			.then(() => console.log('Connected'))
			.catch((err) => console.log(err));

		await sequelize.sync({force: true})
			.then(() => console.log('Database & tables created'))
			.catch((err) => console.log(err));
	} catch (error) {
		console.log(error);
	}
};





export default sequelize;
export { connectToPostGres };