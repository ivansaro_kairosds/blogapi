import 'reflect-metadata';

import { config } from 'dotenv';
config();


import express from 'express';
import { json } from 'body-parser';
import Container from 'typedi';
import passport from 'passport';
import expressPinoLogger from 'express-pino-logger';
import swaggerUI from 'swagger-ui-express';
import swaggerJsdoc, { Options } from 'swagger-jsdoc';


import { authRouter } from './infrastructure/routes/auth.route';
import { PostRouter } from './infrastructure/routes/post.route';
import { offensiveWordRouter } from './infrastructure/routes/offensive-word.route';

import { OffensiveWordRepositoryMongo } from './infrastructure/repositories/offensive-word.repository.mongo';
import { UserRepositoryPG } from './infrastructure/repositories/user.repository.pg';
import { AuthorRepositoryMongo } from './infrastructure/repositories/author.repository.mongo';
import { PostRepositoryMongo } from './infrastructure/repositories/post.repository.mongo';


import { logger } from './infrastructure/config/logger';
import passportMiddleWare from './infrastructure/middlewares/passport';

Container.set('OffensiveWordRepository', new OffensiveWordRepositoryMongo());

Container.set('UserRepository', new UserRepositoryPG());

Container.set('AuthorRepository', new AuthorRepositoryMongo());

Container.set('PostRepository', new PostRepositoryMongo());


console.log('App started');

const app = express();
app.use(json());
app.use(expressPinoLogger(logger));
app.use(offensiveWordRouter);
app.use(authRouter);
app.use(PostRouter);
app.use(passport.initialize());
passport.use(passportMiddleWare);

const port = process.env.SERVER_PORT;

const options: Options = {
	definition: {
		openapi: '3.0.0',
		info: {
			title: 'Blog API',
			version: '1.0.0',
			description: 'Blog API'
		},
		servers: [
			{
				url: `http://localhost:${port}/api`
			}
		]
	},
	apis: ['./src/infraestructure/routes/*.ts']
};
const specs = swaggerJsdoc(options);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs));



export default app;