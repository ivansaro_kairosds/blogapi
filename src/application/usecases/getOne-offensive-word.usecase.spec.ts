jest.mock('../../infrastructure/repositories/offensive-word.repository.mongo', () => {
	return {
		OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
			return {
				get: mockResult
			};
		})
	};
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { WordVO } from '../../domain/vos/offensive-word/word.vo';
import { LevelVO } from '../../domain/vos/offensive-word/level.vo';
import { GetOneOffensiveWordUseCase } from './getOne-offensive-word.usecase';
import { OffensiveWord } from '../../domain/entities/offensive-word.entity';

let repository: OffensiveWordRepositoryMongo;
const mockResult = jest.fn();

describe('Show one offensive word', () => {
	beforeEach(() => {
		repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);
	});
	test('Show one offensive word correctly', async ()=> {
		mockResult.mockResolvedValue(new OffensiveWord(
			{
				id: IdVO.createWithUUID('6c91b899-8dd9-4722-8289-00be42e7881d'),
				word: WordVO.create('Testing'),
				level: LevelVO.create(2)
			})
		);
		const useCase: GetOneOffensiveWordUseCase = Container.get(GetOneOffensiveWordUseCase);
		const id = '6c91b899-8dd9-4722-8289-00be42e7881d';
		const show = await useCase.execute(id);
		expect(show?.word).toBe('Testing');
		expect(show?.level).toBe(2);
		expect(show?.id).toBe(id);
	});

	test('Show all offensive words', async ()=> {
		const id = '6c91b899-8dd9-4722-8289-00be42e7881d';
		try {
			mockResult.mockResolvedValue(null);
			const useCase: GetOneOffensiveWordUseCase = Container.get(GetOneOffensiveWordUseCase);
			const show = await useCase.execute(id);
			expect(show).toBe(null);
		} catch (error) {
			expect(error.code).toBe(404);
			expect(error.message).toBe(`Id ${id} not found`);
		}
		
	});
});