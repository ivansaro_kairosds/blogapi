import { Service } from 'typedi';
import { OffensiveWordService } from '../../domain/services/offensive.word.service';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { IdRequest } from './offensive-word.idrequest';

@Service()
export class DeleteOffensiveWordUseCase {

	constructor(private offensiveWordService: OffensiveWordService) {}
    
	async execute(idRequest: IdRequest): Promise<void> {
		await this.offensiveWordService.delete(IdVO.createWithUUID(idRequest));
	}
	
}