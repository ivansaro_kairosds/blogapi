import { OffensiveWordRequest } from './offensive-word.request';
import { OffensiveWordService } from '../../domain/services/offensive.word.service';
import { OffensiveWordType } from '../../domain/entities/offensive-word.entity';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { WordVO } from '../../domain/vos/offensive-word/word.vo';
import { LevelVO } from '../../domain/vos/offensive-word/level.vo';
import { Service } from 'typedi';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class CreateOffensiveWordUseCase {

	constructor(private offensiveWordService: OffensiveWordService) {}
    
	async execute(offensiveWordRequest: OffensiveWordRequest): Promise<OffensiveWordResponse> {
		const offensiveWordData: OffensiveWordType = {
			id: IdVO.create(),
			word: WordVO.create(offensiveWordRequest.word),
			level: LevelVO.create(offensiveWordRequest.level)
		};
        
		await this.offensiveWordService.persist(offensiveWordData);

		return Promise.resolve({
			id: offensiveWordData.id.value,
			word: offensiveWordData.word.value,
			level: offensiveWordData.level.value
		});
	}
}