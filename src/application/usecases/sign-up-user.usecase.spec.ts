jest.mock('../../infrastructure/repositories/user.repository.pg', () => {
	return {
		UserRepositoryPG: jest.fn().mockImplementation(() => {
			return {
				save: jest.fn().mockImplementation()
			};
		})
	};
});

import 'reflect-metadata';
import { UserRepositoryPG } from '../../infrastructure/repositories/user.repository.pg';
import Container from 'typedi';
import { SignUpUserRequest, SignUpUserUseCase } from './sign-up-user.usecase';
import { IdVO } from '../../domain/vos/id.vo';


describe('User sign-up suite', () => {

	test('Should sign up', async () => {

		const repository = new UserRepositoryPG();
		Container.set('UserRepository', repository);

		const useCase = Container.get(SignUpUserUseCase);
		
		const email = 'test@mail.com';
		const password = 'password';
		const id = IdVO.create();
			
		
		await useCase.execute({id, email, password});
		expect(await repository.save).not.toHaveBeenCalled();
	});
}); 