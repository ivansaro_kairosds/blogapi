jest.mock('../../infrastructure/repositories/offensive-word.repository.mongo', () => {
	return {
		OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
			return {
				update: jest.fn().mockImplementation(() => new OffensiveWord({id: IdVO.createWithUUID('111dec00-1f9b-4cf4-aa61-947e2d53e950'), word: WordVO.create('Testing'), level: LevelVO.create(3)})),
				get: jest.fn().mockImplementation(() => new OffensiveWord({id: IdVO.createWithUUID('111dec00-1f9b-4cf4-aa61-947e2d53e950'), word: WordVO.create('Testing'), level: LevelVO.create(3)}))
			};
		})
	};
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';
import { IdRequest } from './offensive-word.idrequest';
import { UpdateOffensiveWordUseCase } from './update-offensive-word.usecase';
import { OffensiveWordRequest } from './offensive-word.request';
import { OffensiveWord } from '../../domain/entities/offensive-word.entity';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { WordVO } from '../../domain/vos/offensive-word/word.vo';
import { LevelVO } from '../../domain/vos/offensive-word/level.vo';


describe('Show all offensive words', () => {

	test('Show all offensive words', async ()=> {
		const repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);
		const useCase: UpdateOffensiveWordUseCase = Container.get(UpdateOffensiveWordUseCase);
		const id: IdRequest = '111dec00-1f9b-4cf4-aa61-947e2d53e950';
		const offensiveWordRequestMock: OffensiveWordRequest = {
			word: 'Test',
			level: 1
		};
		await useCase.execute(id, offensiveWordRequestMock);
		expect(repository.update).toBeCalled();
	});
});