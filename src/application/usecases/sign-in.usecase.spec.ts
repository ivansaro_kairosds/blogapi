import 'reflect-metadata';
import { UserRepositoryPG } from '../../infrastructure/repositories/user.repository.pg';
import Container from 'typedi';
import { EmailVO } from '../../domain/vos/user/email.vo';
import { PasswordVO } from '../../domain/vos/user/password.vo';
import { SignInUseCase, SignInRequest } from './sign-in.usecase';
import { IdVO } from '../../domain/vos/id.vo';

let repository: UserRepositoryPG;
const mockResult = jest.fn();

jest.mock('../../infrastructure/repositories/user.repository.pg', () => {
	return {
		UserRepositoryPG: jest.fn().mockImplementation(() => {
			return {
				getByEmail: mockResult
			};
		})
	};
});

describe('User sign-in suite', () => {
	beforeEach(() => {
		repository = new UserRepositoryPG();
		Container.set('UserRepository', repository);
	});
	test('Should sign in and return an userData', async () => {
		mockResult.mockResolvedValue( {
			id: IdVO.createWithUUID('e462a7e6-04a5-4f22-965a-af5298331f95'),
			email: EmailVO.create('hola@hola'),
			password: PasswordVO.create('$2b$10$kxeS2ohNHUIB/Bk4fBkksOdQy64EqwVbbBXKBsIwn0bCTQ4gnr8Ky')
		});
		const useCase = Container.get(SignInUseCase);
		const userMock: SignInRequest = {
			email: 'test@mail.com',
			password: 'password'
		};
		await useCase.execute(userMock);
		expect(repository.getByEmail).toHaveBeenCalled();
	});

	test('Should return an error if data is invalid', async () => {
		try {
			mockResult.mockResolvedValue(null);
			const useCase = Container.get(SignInUseCase);
			const userMock: SignInRequest = {
				email: 'test@mail.com',
				password: 'password'
			};
			await useCase.execute(userMock);
			expect(repository.getByEmail).toHaveBeenCalled();
		} catch (error) {
			expect(error.message).toBe('User not found');
			expect(error.code).toBe(404);
		}
        
	});
});