import { UserService } from '../../domain/services/user.service';
import { User, UserType } from '../../domain/entities/user.entity';
import { IdVO } from '../../domain/vos/id.vo';
import { EmailVO } from '../../domain/vos/user/email.vo';
import { PasswordVO } from '../../domain/vos/user/password.vo';
import { Service } from 'typedi';
import { Role, RoleVO } from '../../domain/vos/user/role.vo';


@Service()
export class SignUpUserUseCase {

	constructor(private userService: UserService) {}
    
	async execute(request: SignUpUserRequest): Promise<void> {
		console.log('useCase');
		const user: UserType = {
			id: request.id,
			email: EmailVO.create(request.email),
			password: PasswordVO.create(request.password),
			role: RoleVO.create(Role.USER)
		};

		
		this.userService.persist(new User(user));
	}
}

export type SignUpUserRequest = {
    id: IdVO,
	email: string,
    password: string,
}