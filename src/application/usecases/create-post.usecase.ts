import { Service } from 'typedi';
import { PostService } from '../../domain/services/post.service';
import { IdRequest } from './offensive-word.idrequest';
import { PostRequest } from './post.request';
import { PostType } from '../../domain/entities/post.entity';
import { PostResponse } from './post.response';
import { AuthorService } from '../../domain/services/author.service';
import { IdVO } from '../../domain/vos/id.vo';
import { ExceptionWithCode } from '../../domain/exception-with.code';
import { TitleVO } from '../../domain/vos/post/title.vo';
import { TextVO } from '../../domain/vos/post/text.vo';

@Service()
export class CreatePostUseCase {

	constructor(private postService: PostService, private authorService: AuthorService) {}

	async execute(id: IdRequest, postRequest: PostRequest): Promise<PostResponse> {
		const idAuthor = IdVO.createWithUUID(id);
		const author = await this.authorService.getOneItem(idAuthor);
		if(author === null) {
			throw new ExceptionWithCode(404, 'ID no es válido');
		}
		const postData: PostType = {
			id: IdVO.create(),
			author: author,
			title: TitleVO.create(postRequest.title),
			text: TextVO.create(postRequest.text),
			coments: []
		};

		await this.postService.persist(postData);
        
		return Promise.resolve({
			id: postData.id.value,
			author: postData.author,
			title: postData.title.value,
			text: postData.text.value
		});
	}
}