jest.mock('../../infrastructure/repositories/offensive-word.repository.mongo');

import 'reflect-metadata';
import Container from 'typedi';
import { CreateOffensiveWordUseCase } from './create-offensive-word.usecase';
import { OffensiveWordRequest } from './offensive-word.request';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';

describe('Create offensive word use case', () => {

	it('should create offensive word and persist', () => {

		const repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);

		const useCase = Container.get(CreateOffensiveWordUseCase);
		const offensiveWordRequest: OffensiveWordRequest = {
			word: 'Test',
			level: 3
		};
		useCase.execute(offensiveWordRequest);
		expect(repository.save).toHaveBeenCalled();
	});

	it('should create offensive word and return a word', async () => {

		const repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);

		const useCase = Container.get(CreateOffensiveWordUseCase);
		const offensiveWordRequest: OffensiveWordRequest = {
			word: 'Test',
			level: 3
		};
		const show = await useCase.execute(offensiveWordRequest);
		expect(show.word).toBe('Test');
		expect(show.level).toBe(3);
	});
});