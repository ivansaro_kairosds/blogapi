export type OffensiveWordResponse = {
    word: string,
    level: number,
    id: string
}