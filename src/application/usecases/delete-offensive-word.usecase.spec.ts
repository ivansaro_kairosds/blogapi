jest.mock('../../infrastructure/repositories/offensive-word.repository.mongo', () => {
	return {
		OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
			return {
				get: mockResult,
				delete: jest.fn().mockImplementation()
			};
		})
	};
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';
import { DeleteOffensiveWordUseCase } from './delete-offensive-word.usecase';
import { IdRequest } from './offensive-word.idrequest';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { WordVO } from '../../domain/vos/offensive-word/word.vo';
import { LevelVO } from '../../domain/vos/offensive-word/level.vo';
import { OffensiveWord } from '../../domain/entities/offensive-word.entity';

let repository: OffensiveWordRepositoryMongo;
const mockResult = jest.fn();
describe('Delete offensive word use case', () => {
	beforeEach(() => {
		repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);
	});
	it('should delete offensive word', async () => {
		mockResult.mockResolvedValue(new OffensiveWord(
			{
				id: IdVO.createWithUUID('6c91b899-8dd9-4722-8289-00be42e7881d'),
				word: WordVO.create('Testing'),
				level: LevelVO.create(2)
			})
		);
		const repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);

		const useCase = Container.get(DeleteOffensiveWordUseCase);
		const id: IdRequest = '6c91b899-8dd9-4722-8289-00be42e7881d';
		await useCase.execute(id);

		expect(repository.delete).toBeCalled();
	});

	it('should delete offensive word', async () => {
		const id: IdRequest = '6c91b899-8dd9-4722-8289-00be42e78813';
		try {
			mockResult.mockResolvedValue(null);
			const repository = new OffensiveWordRepositoryMongo();
			Container.set('OffensiveWordRepository', repository);

			const useCase = Container.get(DeleteOffensiveWordUseCase);
			await useCase.execute(id);
		} catch (error) {
			expect(error.code).toBe(404);
			expect(error.message).toBe(`Id ${id} not found`);
		}
	});

	
});

//expect(repository.delete).toHaveBeenCalled();
