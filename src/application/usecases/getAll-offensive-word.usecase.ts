import { Service } from 'typedi';
import { OffensiveWord } from '../../domain/entities/offensive-word.entity';
import { OffensiveWordService } from '../../domain/services/offensive.word.service';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class GetAllOffensiveWordUseCase {

	constructor(private offensiveWordService: OffensiveWordService) {}
    
	async execute(): Promise<OffensiveWordResponse[]> {
		const allItems: OffensiveWord[] = await this.offensiveWordService.getAllItems();
		const offensiveWordResponse: OffensiveWordResponse[] = allItems.map((ob: OffensiveWord) => {
			return {word: ob.word.value, level: ob.level.value, id: ob.id.value};
		});
		return offensiveWordResponse; 
	}
}