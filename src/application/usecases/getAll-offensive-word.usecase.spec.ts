import 'reflect-metadata';
import Container from 'typedi';
import { GetAllOffensiveWordUseCase } from './getAll-offensive-word.usecase';
import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { WordVO } from '../../domain/vos/offensive-word/word.vo';
import { LevelVO } from '../../domain/vos/offensive-word/level.vo';
import { OffensiveWord } from '../../domain/entities/offensive-word.entity';

const mockObj1 = new OffensiveWord({id: IdVO.createWithUUID('111dec00-1f9b-4cf4-aa61-947e2d53e950'), word: WordVO.create('Testing'), level: LevelVO.create(3)});
const mockObj2 = new OffensiveWord({id: IdVO.create(), word: WordVO.create('Testing'), level: LevelVO.create(5)});

jest.mock('../../infrastructure/repositories/offensive-word.repository.mongo', () => {
	return {
		OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
			return {
				getAll: jest.fn().mockImplementation(() => [mockObj1, mockObj2])
			};
		})
	};
});

describe('Show all offensive words', () => {

	test('Show all offensive words', async ()=> {
		const repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);
		const useCase: GetAllOffensiveWordUseCase = Container.get(GetAllOffensiveWordUseCase);
		const words = await useCase.execute();
		expect(words[0].word).toBe('Testing');
		expect(words[1].level).toBe(5);
		expect(words[0].id).toBe('111dec00-1f9b-4cf4-aa61-947e2d53e950');
		expect(words[1].id).toBeDefined();
	});
});