import { Service } from 'typedi';
import { OffensiveWordService } from '../../domain/services/offensive.word.service';
import { OffensiveWordRequest } from './offensive-word.request';
import { IdRequest } from './offensive-word.idrequest';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class UpdateOffensiveWordUseCase {

	constructor(private offensiveWordService: OffensiveWordService) {}

	async execute(id: IdRequest, offensiveWordRequest: OffensiveWordRequest): Promise<OffensiveWordResponse> {
		const result = await this.offensiveWordService.update(id, offensiveWordRequest );
		const offensiveWordResponse: OffensiveWordResponse = {
			id: result.id.value,
			level: result.level.value,
			word: result.word.value
		}; 
		return offensiveWordResponse;
	}
}