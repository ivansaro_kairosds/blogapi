import { Service } from 'typedi';
import { Author, AuthorType } from '../../domain/entities/author.entity';
import { AuthorNameVO } from '../../domain/vos/author/authorName.vo';
import { AuthorNicknameVO } from '../../domain/vos/author/authorNickname.vo';
import { AuthorService } from '../../domain/services/author.service';
import { IdVO } from '../../domain/vos/id.vo';

@Service()
export class SignUpAuthorCase {

	constructor(private authorService: AuthorService) {}

	async execute(request: SignUpAuthorRequest): Promise<void> {
		const author: AuthorType = {
			id: request.id,
			name: AuthorNameVO.create(request.name),
			nickname: AuthorNicknameVO.create(request.nickname)
		};  
		await this.authorService.persist(new Author(author));
	}
}

export type SignUpAuthorRequest = {
    name: string,
    nickname: string,
    id: IdVO
}