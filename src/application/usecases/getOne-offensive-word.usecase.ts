import { Service } from 'typedi';
import { OffensiveWordService } from '../../domain/services/offensive.word.service';
import { IdRequest } from './offensive-word.idrequest';
import { IdVO } from '../../domain/vos/offensive-word/id.vo';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class GetOneOffensiveWordUseCase {

	constructor(private offensiveWordService: OffensiveWordService) {}
    
	async execute(idRequest: IdRequest): Promise<OffensiveWordResponse | null> {
		const oneItem = await this.offensiveWordService.getOneItem(IdVO.createWithUUID(idRequest));
		if(oneItem === null){return null;}
		const offensiveWordResponse: OffensiveWordResponse = {
			id: oneItem.id.value,
			level: oneItem.level.value,
			word: oneItem.word.value
		};
		return offensiveWordResponse;  
	}
} 
