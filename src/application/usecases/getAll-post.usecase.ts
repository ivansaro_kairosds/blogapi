import { Service } from 'typedi';
import { PostService } from '../../domain/services/post.service';
import { PostResponse } from './post.response';
import { Post } from '../../domain/entities/post.entity';

@Service()
export class GetAllPostUseCase {

	constructor(private postService: PostService) {}

	async execute(): Promise<PostResponse[]> {
		const allPost: Post[] = await this.postService.getAllItems();
		const postResponse: PostResponse[] = allPost.map((ob: Post) => {
			return {id: ob.id.value, author: ob.author, title: ob.title.value, text: ob.text.value, comments: ob.comments};
		});
		return postResponse;
	}
}