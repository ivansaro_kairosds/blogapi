import { Author } from '../../domain/entities/author.entity';

export type PostResponse = {
    id: string,
    title: string,
    text: string,
    author: Author
}