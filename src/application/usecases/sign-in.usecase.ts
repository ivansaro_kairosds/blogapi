import { UserService } from '../../domain/services/user.service';
import { EmailVO } from '../../domain/vos/user/email.vo';
import { ExceptionWithCode } from '../../domain/exception-with.code';
import { PasswordVO } from '../../domain/vos/user/password.vo';
import jwt from 'jsonwebtoken';
import { Service } from 'typedi';
import { json } from 'body-parser';

@Service()
export class SignInUseCase {

	constructor(private userService: UserService) {}

	async execute(request: SignInRequest): Promise<string | null> {
		
		const user = await this.userService.getByEmail(EmailVO.create(request.email));
		if(!user) {
			throw new ExceptionWithCode(404, 'User not found');
		}

		const plainPassword = PasswordVO.create(request.password);
		const isValid = await this.userService.isValidPassword(plainPassword, user);
		
		const secret = process.env.CRPYT_SECRET;

		if(isValid) {
			return jwt.sign({email: user.email.value}, `${secret}`, {
				expiresIn: 86400
			});
		}
		return null;
	}

}

export type SignInRequest = {
    email: string;
    password: string;
}