import { EmailVO } from '../vos/user/email.vo';
import { PasswordVO } from '../vos/user/password.vo';
import { User } from './user.entity';
import { Role, RoleVO } from '../vos/user/role.vo';
import { IdVO } from '../vos/id.vo';
describe('User entity test suite', () => {

	test('Should create a User', () => {
		const id = IdVO.create();
		const email = EmailVO.create('prueba@mail.com');
		const password = PasswordVO.create('password');
		const role = RoleVO.create(Role.USER);
		const user: User = new User({id, email, password, role});

		expect(user).toBeDefined();
		expect(user.email.value).toBe('prueba@mail.com');
		expect(user.password.value).toBe('password');
		expect(user.id.value).toMatch(/^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i);
	});
});