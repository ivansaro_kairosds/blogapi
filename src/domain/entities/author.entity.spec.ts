import { IdVO } from '../vos/id.vo';
import { AuthorNameVO } from '../vos/author/authorName.vo';
import { AuthorNicknameVO } from '../vos/author/authorNickname.vo';
import { Author } from './author.entity';

describe('Author entity test suite', () => {


	test('Should create', () => {
		const id =  IdVO.create();
		const name = AuthorNameVO.create('Pepito');
		const nickname = AuthorNicknameVO.create('Pep');

		const author: Author = new Author({id, name, nickname});

		expect(author).toBeDefined();
		expect(author.name.value).toBe('Pepito');
		expect(author.nickname.value).toBe('Pep');
		expect(author.id.value).toMatch(/^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i);
	});
});