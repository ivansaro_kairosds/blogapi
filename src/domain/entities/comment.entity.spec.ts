import { IdVO } from '../vos/id.vo';
import { AuthorNicknameVO } from '../vos/comment/authorNickname.vo';
import { ContentVO } from '../vos/comment/content.vo';
import { Comment } from './comment.entity';
describe('Comment entity test suite', () => {

	test('should create', () => {

		const id = IdVO.create();
		const nickname = AuthorNicknameVO.create('Jos');
		const content = ContentVO.create('Este es mi comentario, hay otros mejores pero este es el mio');

		const comment: Comment = new Comment({id, nickname, content});

		expect(comment).toBeDefined();
		expect(comment.nickname.value).toBe('Jos');
		expect(comment.content.value).toBe('Este es mi comentario, hay otros mejores pero este es el mio');
		expect(comment.id.value).toMatch(/^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i);
	});
});