import { AuthorNicknameVO } from '../vos/comment/authorNickname.vo';
import { ContentVO } from '../vos/comment/content.vo';
import { IdVO } from '../vos/id.vo';

export type CommentType = {
    id: IdVO;
    nickname: AuthorNicknameVO;
    content: ContentVO;
}

export class Comment {

	constructor(private comment: CommentType) {}

	get id(): IdVO {
		return this.comment.id;
	}

	get nickname(): AuthorNicknameVO {
		return this.comment.nickname;
	}

	get content(): ContentVO {
		return this.comment.content;
	}
    
}