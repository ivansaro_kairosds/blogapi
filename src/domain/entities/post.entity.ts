import { IdVO } from '../vos/id.vo';
import { TitleVO } from '../vos/post/title.vo';
import { TextVO } from '../vos/post/text.vo';
import { Comment } from './comment.entity';
import { Author } from './author.entity';

export type PostType = {
    id: IdVO;
	author: Author;
    title: TitleVO;
    text: TextVO;
    coments: Comment[]
}

export class Post {

	constructor(private post: PostType) {}

	get id(): IdVO {
		return this.post.id;
	}

	get author(): Author {
		return this.post.author;
	}

	get title(): TitleVO {
		return this.post.title;
	}

	get text(): TextVO {
		return this.post.text;
	}

	get comments(): Comment[] {
		return this.post.coments;
	}
}