import { IdVO } from '../vos/offensive-word/id.vo';
import { WordVO } from '../vos/offensive-word/word.vo';
import { LevelVO } from '../vos/offensive-word/level.vo';
import { OffensiveWord } from './offensive-word.entity';

describe('Offensive word entity test suite', () => {

	test('Should create an offensive word', () => {
		const id = IdVO.create();
		const word = WordVO.create('Word');
		const level = LevelVO.create(4);
		const offensiveWord: OffensiveWord = new OffensiveWord({id, level, word});

		expect(offensiveWord).toBeDefined();
		expect(offensiveWord.word.value).toBe('Word');
		expect(offensiveWord.level.value).toBe(4);
	});

	/* test('Should throw an error if some field is incorrect', () => {
		const id = IdVO.create();
		const word = WordVO.create('Word');
		const level = LevelVO.create(7);
		const offensiveWord: OffensiveWord = new OffensiveWord({id, level, word});

		expect(() => offensiveWord).toThrowError('ExceptionWithCode: 7 no es válido. Tiene que estar entre 1 y 5.');
		
	}); */
});
