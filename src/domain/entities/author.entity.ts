import { IdVO } from '../vos/id.vo';
import { AuthorNameVO } from '../vos/author/authorName.vo';
import { AuthorNicknameVO } from '../vos/author/authorNickname.vo';

export type AuthorType = {
    id: IdVO;
    name: AuthorNameVO;
    nickname: AuthorNicknameVO
}

export class Author {

	constructor(private author: AuthorType) {}

	get id(): IdVO {
		return this.author.id;
	}

	get name(): AuthorNameVO {
		return this.author.name;
	}

	get nickname(): AuthorNicknameVO {
		return this.author.nickname;
	}
}