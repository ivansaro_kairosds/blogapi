import { User } from '../entities/user.entity';
import { EmailVO } from '../vos/user/email.vo';

export interface UserRepository {

    save(user: User): void;

    getByEmail(email: EmailVO): Promise<User | null>

    deleteAll(): Promise<void>
}