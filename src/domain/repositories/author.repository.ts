import { Author } from '../entities/author.entity';
import { IdVO } from '../vos/id.vo';

export interface AuthorRepository {
    save(author: Author): void

    get(id: IdVO): Promise<Author | null>
} 