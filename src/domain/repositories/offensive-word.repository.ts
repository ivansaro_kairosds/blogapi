import { OffensiveWord } from '../entities/offensive-word.entity';
import { IdVO } from '../vos/offensive-word/id.vo';
export interface OffensiveWordRepository {

    save(offensiveWord: OffensiveWord): void;

    delete(id: IdVO): void;

    update(offensiveWord: OffensiveWord): Promise<OffensiveWord>

    getAll(): Promise<OffensiveWord[]>

    get(id: IdVO): Promise<OffensiveWord | null>

    deleteAll(): Promise<void>
}