export class ContentVO {

	get value(): string {
		return this.content;
	}

	private constructor(private content: string) {}

	static create(content: string): ContentVO {
		if(content.length < 10 || content.length > 200) {
			throw new Error('El contenido del comentario no es válido. Debe tener una longitud entre 10 y 200 caracteres');
		}

		return new ContentVO(content);
	}
}