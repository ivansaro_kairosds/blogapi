import { ContentVO } from './content.vo';

describe('Content VO', () => {

	it('should create', () => {
		const sampleContent = 'I absolute love this post!';
		const name = ContentVO.create(sampleContent);

		expect(name.value).toEqual(sampleContent);
	});

	it('should throw error if content.length is lower than 10', () => {
		const shortContent = 'Kudos';

		expect(() => ContentVO.create(shortContent)).toThrow('El contenido del comentario no es válido. Debe tener una longitud entre 10 y 200 caracteres');
	});

	it('should throw error if content.length is higher than 200', () => {
		const longContent = 'In a hole in the ground there lived a hobbit. Not a nasty, dirty, wet hole, filled with the ends of worms and an oozy smell, nor yet a dry, bare, sandy hole with nothing in it to sit down on or to eat: it was a hobbit-hole, and that means comfort.';

		expect(() => ContentVO.create(longContent)).toThrow('El contenido del comentario no es válido. Debe tener una longitud entre 10 y 200 caracteres');
	});
});