import { PasswordVO } from './password.vo';
describe('Word VO', () => {

	it('should create', () => {
		const password = 'World';
		const newPassword: PasswordVO = PasswordVO.create(password);
		expect(newPassword.value).toEqual(password);
	});
});