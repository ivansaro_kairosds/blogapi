import { EmailVO } from './email.vo';
describe('Word VO', () => {

	it('should create', () => {
		const email = 'World';
		const newEmail: EmailVO = EmailVO.create(email);
		expect(newEmail.value).toEqual(email);
	});
});