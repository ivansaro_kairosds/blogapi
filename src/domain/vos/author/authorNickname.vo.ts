import { ExceptionWithCode } from '../../exception-with.code';
export class AuthorNicknameVO {

	get value(): string {
		return this.authorNickname;
	}

	private constructor(private authorNickname: string) {}

	static create(authorNickname: string): AuthorNicknameVO {
		if(authorNickname.length < 3 || authorNickname.length > 10) {
			throw new ExceptionWithCode(400, `${authorNickname} no es válido. Debe tener una longitud entre 3 y 10 caracteres`);
		}

		return new AuthorNicknameVO(authorNickname);
	}
}