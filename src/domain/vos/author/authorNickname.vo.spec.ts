import { AuthorNicknameVO } from './authorNickname.vo';
describe('Author nickname VO', () => {

	it('should create', () => {
		const sampleName = 'Trout';
		const name = AuthorNicknameVO.create(sampleName);

		expect(name.value).toEqual(sampleName);
	});

	it('should throw error if name.length is lower than 3', () => {
		const shortName = 'Mo';

		expect(() => AuthorNicknameVO.create(shortName)).toThrow(`${shortName} no es válido. Debe tener una longitud entre 3 y 10 caracteres`);
	});

	it('should throw error if name.length is higher than 10', () => {
		const longName = 'Apu Nahasapeemapetilon';

		expect(() => AuthorNicknameVO.create(longName)).toThrow(`${longName} no es válido. Debe tener una longitud entre 3 y 10 caracteres`);
	});
});