import { ExceptionWithCode } from '../../exception-with.code';
export class AuthorNameVO {

	get value(): string {
		return this.authorName;
	}

	private constructor(private authorName: string) {}

	static create(authorName: string): AuthorNameVO {
		if(authorName.length < 5 || authorName.length > 30) {
			throw new ExceptionWithCode(400, `${authorName} no es válido. Debe tener una longitud entre 5 y 30 caracteres`);
		}

		return new AuthorNameVO(authorName);
	}
}