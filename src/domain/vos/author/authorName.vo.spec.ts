import { AuthorNameVO } from './authorName.vo';
describe('Author name VO', () => {

	it('should create', () => {
		const sampleName = 'Trout Kilgore';
		const name = AuthorNameVO.create(sampleName);

		expect(name.value).toEqual(sampleName);
	});

	it('should throw error if name.length is lower than 5', () => {
		const shortName = 'Jay';

		expect(() => AuthorNameVO.create(shortName)).toThrow(`${shortName} no es válido. Debe tener una longitud entre 5 y 30 caracteres`);
	});

	it('should throw error if name.length is higher than 30', () => {
		const longName = 'Hubert Blaine Wolfeschlegelsteinhausenbergerdorff Sr.';

		expect(() => AuthorNameVO.create(longName)).toThrow(`${longName} no es válido. Debe tener una longitud entre 5 y 30 caracteres`);
	});
});