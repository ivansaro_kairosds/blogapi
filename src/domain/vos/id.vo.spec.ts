import { IdVO } from './id.vo';
describe('IdVO', () => {

	test('Should create', () => {
		const idVO = IdVO.create();
		expect(idVO.value).toMatch(/^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i);
	});

	test('Should validate', () => {
		const id = '8daf6434-d910-4a29-8df0-837b8ac9d2e4';
		const idVO = IdVO.createWithUUID(id);
		expect(idVO.value).toMatch(/^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i);
		expect(idVO.value).toBe(id);
	});

	it('shoul throw error if id is invalid', () => {
		const invalidId = '8daf6434-d910-4a29-8df0-837b8ac9d2e';
		expect(() => IdVO.createWithUUID(invalidId)).toThrowError('ID no es un UUID');
	});
});