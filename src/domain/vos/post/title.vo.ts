export class TitleVO {

	get value(): string {
		return this.title;
	}

	private constructor(private title: string) {}

	static create(title: string): TitleVO {
		if(title.length < 5 || title.length > 30) {
			throw new Error (`${title} no es válido. Debe tener una longitud entre 5 y 30 caracteres`);
		}

		return new TitleVO(title);
	}
}