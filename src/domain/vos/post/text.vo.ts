export class TextVO {

	get value(): string {
		return this.text;
	}

	private constructor(private text: string) {}

	static create(text: string): TextVO {
		if(text.length < 50 || text.length > 300) {
			throw new Error('El texto no es válido. Debe tener una longitud entre 50 y 3000 caracteres');
		}

		return new TextVO(text);
	}
}