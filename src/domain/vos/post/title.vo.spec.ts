import { TitleVO } from './title.vo';
describe('Title VO', () => {

	it('should create', () => {
		const sampleTitle = 'Story of two cities';
		const name = TitleVO.create(sampleTitle);

		expect(name.value).toEqual(sampleTitle);
	});

	it('should throw error if title.length is lower than 5', () => {
		const shortTitle = 'Main';

		expect(() => TitleVO.create(shortTitle)).toThrow(`${shortTitle} no es válido. Debe tener una longitud entre 5 y 30 caracteres`);
	});

	it('should throw error if title.length is higher than 30', () => {
		const longName = 'The Personal History, Adventures, Experience and Observation of David Copperfield the Younger of Blunderstone Rookery';

		expect(() => TitleVO.create(longName)).toThrow(`${longName} no es válido. Debe tener una longitud entre 5 y 30 caracteres`);
	});
});