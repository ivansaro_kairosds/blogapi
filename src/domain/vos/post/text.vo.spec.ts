import { TextVO } from './text.vo';
describe('Text VO', () => {

	it('should create', () => {
		const sampleText = 'A Christmas Carol has never been out of print and has been translated into several languages; the story has been adapted many times for film, stage, opera and other media';
		const text = TextVO.create(sampleText);

		expect(text.value).toEqual(sampleText);
	});

	it('should throw error if text.length is lower than 50', () => {
		const shortText = 'A Christmas Carol captured the zeitgeist.';

		expect(() => TextVO.create(shortText)).toThrow('El texto no es válido. Debe tener una longitud entre 50 y 3000 caracteres');
	});

	it('should throw error if text.length is higher than 300', () => {
		const longName = 'It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of Light, it was the season of Darkness, it was the spring of hope, it was the winter of despair, we had everything before us, we had nothing before us, we were all going direct to Heaven, we were all going direct the other way—in short, the period was so far like the present period, that some of its noisiest authorities insisted on its being received, for good or for evil, in the superlative degree of comparison only.';

		expect(() => TextVO.create(longName)).toThrow('El texto no es válido. Debe tener una longitud entre 50 y 3000 caracteres');
	});
});