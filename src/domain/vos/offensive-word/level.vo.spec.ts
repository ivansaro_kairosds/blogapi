import { LevelVO } from './level.vo';
describe('Level VO', () => {

	it('should create', () => {
		const minLevel = 1;
		const maxLevel = 5;
		const levelMin = LevelVO.create(minLevel);
		const levelMax = LevelVO.create(maxLevel);

		expect(levelMin.value).toEqual(minLevel);
		expect(levelMax.value).toEqual(maxLevel);
	});

	it('should throw error if level is greater than 5', () => {
		const invalidLevel = 6;

		expect(() => LevelVO.create(invalidLevel)).toThrow(`${invalidLevel} no es válido. Tiene que estar entre 1 y 5.`);
	});

	it('should throw error if level is lower than 1', () => {
		const invalidLevel = 0;

		expect(() => LevelVO.create(invalidLevel)).toThrow(`${invalidLevel} no es válido. Tiene que estar entre 1 y 5.`);
	});
});