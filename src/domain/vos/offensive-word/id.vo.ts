import { v4, validate } from 'uuid';
import { IdRequest } from '../../../application/usecases/offensive-word.idrequest';
import { ExceptionWithCode } from '../../exception-with.code';


export class IdVO {

	
	get value(): string {
		return this.id;
	}

	private constructor(private id: string) {}

	static create(): IdVO {
		return new IdVO(v4());
	}

	static createWithUUID(uuid: IdRequest): IdVO {
		if(!validate(uuid)) {
			throw new ExceptionWithCode(406, 'ID no es un UUID');
		}
		return new IdVO(uuid);
	}
}