import { v4, validate } from 'uuid';
import { IdRequest } from '../../application/usecases/offensive-word.idrequest';


export class IdVO {

	
	get value(): string {
		return this.id;
	}

	private constructor(private id: string) {}

	static create(): IdVO {
		return new IdVO(v4());
	}

	static createWithUUID(uuid: IdRequest): IdVO {
		if(!validate(uuid)) {
			throw new Error('ID no es un UUID');
		}
		return new IdVO(uuid);
	}
}