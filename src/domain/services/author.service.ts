import { Inject, Service } from 'typedi';
import { AuthorRepository } from '../repositories/author.repository';
import { Author } from '../entities/author.entity';
import { IdVO } from '../vos/id.vo';

@Service()
export class AuthorService {

	constructor(@Inject('AuthorRepository') private authorRepository: AuthorRepository) {}

	async persist(author: Author): Promise<void> {
		try {
			const AuthorEntity = new Author(author);
			return this.authorRepository.save(AuthorEntity); 
		} catch (error) {
			console.log(error);
		}
	}

	async getOneItem(id: IdVO): Promise<Author | null> {
		const result = await this.authorRepository.get(id);
		return result;
	}
}  