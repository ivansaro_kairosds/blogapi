import { Inject, Service } from 'typedi';
import { PostType, Post } from '../entities/post.entity';
import { PostRepository } from '../repositories/post.repository';

@Service()
export class PostService {

	constructor(@Inject('PostRepository') private postRepository: PostRepository) {}

	async persist(post: PostType): Promise<void> {
		const Postentity = new Post(post);
		return this.postRepository.save(Postentity);
	}

	async getAllItems(): Promise<Post[]> {
		return this.postRepository.getAll();
	}
}