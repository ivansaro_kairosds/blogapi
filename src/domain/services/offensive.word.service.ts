import { OffensiveWordType, OffensiveWord } from '../entities/offensive-word.entity';
import { OffensiveWordRepository } from '../repositories/offensive-word.repository';
import { IdVO } from '../vos/offensive-word/id.vo';
import { OffensiveWordRequest } from '../../application/usecases/offensive-word.request';
import { Inject, Service } from 'typedi';
import { IdRequest } from '../../application/usecases/offensive-word.idrequest';
import { WordVO } from '../vos/offensive-word/word.vo';
import { LevelVO } from '../vos/offensive-word/level.vo';
import { ExceptionWithCode } from '../exception-with.code';


@Service()
export class OffensiveWordService {

	constructor(@Inject('OffensiveWordRepository') private offensiveWordRepository: OffensiveWordRepository) {}
    
	async persist(offensiveWord: OffensiveWordType): Promise<void> {
		const OffensiveWordEntity = new OffensiveWord(offensiveWord);
		return this.offensiveWordRepository.save(OffensiveWordEntity);
	}

	async delete(id: IdVO): Promise<void> {
		await this.checkIfIDExists(id);
		this.offensiveWordRepository.delete(id);
	}

	async update(id: IdRequest, offensiveWordRequest: OffensiveWordRequest): Promise<OffensiveWord>{
		const uuid = IdVO.createWithUUID(id);
		await this.checkIfIDExists(uuid);
		const originalOffensiveWord = await this.offensiveWordRepository.get(uuid);
		const offensiveWordMerge: OffensiveWordType = {
			id: uuid,
			word: WordVO.create(offensiveWordRequest.word ?? originalOffensiveWord?.word.value),
			level: LevelVO.create(offensiveWordRequest.level ?? originalOffensiveWord?.level.value),
		};
		return await this.offensiveWordRepository.update(new OffensiveWord(offensiveWordMerge));
	}

	async getAllItems(): Promise<OffensiveWord[]> {
		return this.offensiveWordRepository.getAll();
	}

	async getOneItem(Id: IdVO): Promise<OffensiveWord | null> {
		const result = this.offensiveWordRepository.get(Id);
		return result;
	}

	private async checkIfIDExists(id: IdVO): Promise<void> {
		const offensiveWord = await this.getOneItem(id);
		if (!offensiveWord) {
			throw new ExceptionWithCode(404, `Id ${id.value} not found`);
		}
	} 
}	

