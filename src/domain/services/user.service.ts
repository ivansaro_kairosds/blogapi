import bcrypt from 'bcrypt';

import { Inject, Service } from 'typedi';
import { User, UserType } from '../entities/user.entity';
import { UserRepository } from '../repositories/user.repository';
import { PasswordVO } from '../vos/user/password.vo';
import { EmailVO } from '../vos/user/email.vo';

@Service()
export class UserService {

	constructor(@Inject('UserRepository') private UserRepository: UserRepository) {}

	async isValidPassword(password: PasswordVO, user: User): Promise<boolean> {
		return bcrypt.compare(password.value, user.password.value);
	}
	async persist(user: User): Promise<void> {
		const salt = process.env.HASH;
		const SALT = parseInt(salt!);
		
		console.log('servicio');
		const hash = await bcrypt.hash(user.password.value, SALT);
		const encryptedPassword = PasswordVO.create(hash);
		const newUser: UserType = {
			id: user.id,
			email: user.email,
			password: encryptedPassword,
			role: user.role
		};
		this.UserRepository.save(new User(newUser));
	}

	async getByEmail(email: EmailVO): Promise<User | null> {
		return this.UserRepository.getByEmail(email);
	}
}