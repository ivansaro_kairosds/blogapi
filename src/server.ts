import app from './app';
import { populateDatabases } from './infrastructure/config/populate';
import { connectToPostGres } from './infrastructure/config/postgresql';

import { connectToDB } from './infrastructure/config/mongo';
import './infrastructure/config/postgresql';

connectToDB();


const port = process.env.SERVER_PORT;

app.listen(port, async () => {
	console.log('Server started');
	await connectToPostGres();
	populateDatabases();
});